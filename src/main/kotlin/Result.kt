data class Result(
    val month: Int,
    val pendingTransaction: Int,
    val blockedTransaction: Int,
    val income: Float,
    val expenses: Float,
    val categoryList: HashMap<String, Float>
)