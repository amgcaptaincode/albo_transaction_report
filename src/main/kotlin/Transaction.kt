import kotlinx.serialization.Serializable

@Serializable
data class Transaction(
    val uuid: Long,
    val description: String,
    val category: String,
    val operation: String,
    val amount: Float,
    val status: String,
    val creation_date: String
)