import java.time.LocalDate
import java.util.*

data class Transaction2(
    val uuid: Long,
    val description: String,
    val category: String,
    val operation: String,
    val amount: Float,
    val status: String,
    val creationDate: LocalDate,
)
