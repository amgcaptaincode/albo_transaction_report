import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.io.File
import java.io.InputStream
import java.nio.charset.Charset
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

fun main(args: Array<String>) {

    val report = report(getTransactionGroupedByMonth())

    generateReport(report)

}

private fun readFile(): String {
    val pathFile = "./src/main/resources/transaction.json"

    val myFile = File(pathFile)

    val ins: InputStream = myFile.inputStream()

    return ins.readBytes().toString(Charset.defaultCharset())
}

private fun getTransactionList(): List<Transaction> {
    return Json.decodeFromString(readFile())
}

private fun convert(transactions: List<Transaction>): List<Transaction2> {
    val transactions2 = arrayListOf<Transaction2>()
    transactions.forEach { it ->

        val creationDate = it.creation_date
        val formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.ENGLISH)
        val date = LocalDate.parse(creationDate, formatter)

        with(it) {
            val transaction2 = Transaction2(
                uuid,
                description,
                category,
                operation,
                amount,
                status,
                date
            )
            transactions2.add(transaction2)
        }

    }

    transactions2.sortByDescending { it.creationDate }
    transactions2.reverse()
    return transactions2

}


private fun getTransactionGroupedByMonth(): HashMap<Int, List<Transaction2>> {
    val transactionsList = convert(getTransactionList())

    var currentMonth = 1

    val transactionsMap = hashMapOf<Int, List<Transaction2>>()

    var transactionsByMonth = arrayListOf<Transaction2>()

    val iterator = transactionsList.iterator()
    while (iterator.hasNext()) {
        val transaction = iterator.next()

        if (currentMonth == transaction.creationDate.monthValue) {
            transactionsByMonth.add(transaction)
            if (!iterator.hasNext()) {
                transactionsMap[currentMonth] = transactionsByMonth
            }
        } else {
            transactionsMap[currentMonth] = transactionsByMonth
            transactionsByMonth = arrayListOf()
            transactionsByMonth.add(transaction)
            currentMonth = transaction.creationDate.monthValue
        }
    }

    return transactionsMap

}

private fun report(transactionsGrouped: HashMap<Int, List<Transaction2>>): HashMap<Int, Result> {

    val results = hashMapOf<Int, Result>()

    transactionsGrouped.forEach { (month, transactions) ->

        val countPending = transactions.count { it.status == "pending" }
        val countRejected = transactions.count { it.status == "rejected" }

        var totalAmountIncome = 0.0f
        var totalAmountExpenses = 0.0f

        val categories = hashMapOf<String, Float>()

        transactions.forEach {
            with(it) {
                if (operation == "in" && status == "done") {
                    totalAmountIncome += amount
                }
                if (operation == "out") {
                    if (status == "done") {
                        totalAmountExpenses += amount
                    }
                    if (categories[category] != null) {
                        categories[category] = categories[category]!!.plus(amount)
                    } else {
                        categories[category] = amount
                    }
                }
            }
        }

        categories.forEach { (category, value) ->
            categories.replace(category, totalAmountExpenses / value)
        }

        val categoriesSorted = categories.toList().sortedBy { (_, value) -> value}.reversed().toMap()


        val result = Result(
            month,
            countPending,
            countRejected,
            totalAmountIncome,
            totalAmountExpenses,
            categoriesSorted as HashMap<String, Float>
        )
        results[month] = result
    }

    return results

}

private fun generateReport(results: HashMap<Int, Result>) {

    results.forEach { (month, result) ->

        with(result) {
            println(getMonths()[month - 1])
            println("   $pendingTransaction transacciones pendientes")
            println("   $blockedTransaction transacciones bloqueadas\n")
            val strIncome = String.format("%.2f", income)
            val strExpenses = String.format("%.2f", expenses)
            println("   $$strIncome ingresos\n")
            println("   $${strExpenses} gastos\n")

            categoryList.forEach { (category, value) ->
                if (value != 0.0f) {
                    val percentage = String.format("%.2f", value)
                    println("       $category   %$percentage")
                }
            }
            println("\n\n")

        }
    }

}

private fun getMonths(): ArrayList<String> {

    return arrayListOf(
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre"
    )

}











